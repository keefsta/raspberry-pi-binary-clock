#!/usr/bin/env python

import time
import RPi.GPIO as GPIO
import threading
#functionally
# get time
# convert to binary string
# convert to gpio on/offs
# send to pins

GPIO.setmode(GPIO.BCM)

pins=[]
pins.append(17)
pins.append(18)
pins.append(22)
pins.append(23)
pins.append(24)
pins.append(25)

pins.reverse()

def tick():
    for index in range(len(pins)):
        GPIO.setup(pins[index],GPIO.OUT)

    now=time.localtime()
    mins=now.tm_sec+64 #adding 64 makes sure the binary string representation is long enough with out changing the lower bits

    binMins=bin(mins)[-6:] #converts to binary string and keeps the 6 bits we need

    for index in range(len(pins)):
        GPIO.output(pins[index],int(binMins[index]))

    threading.Timer(1, tick).start()

#    GPIO.cleanup()

# TODO handle ^C interuption and tidy up the GPIO on exit
tick()


